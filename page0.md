## Les modules MadJoh vous facilitent l'intégration de l'API MadJoh ##

Les modules MadJoh permettant le raccordement entre votre front et votre back utilisent tous le module AJAX. C'est ce module qui permet d'envoyer des requêtes vers votre serveur en lui spécifiant les paramètres exigés par votre API.
Il possède 3 méthodes principales :

- post() permet d'envoyer des requêtes AJAX classiques en post vers votre serveur.
- postAuth() fonctionne exactement comme post mais s'occupe lui même d'ajouter à vos paramètres de requête les attributs mail et token qui permettent à vos utilisateurs de s'identifier de manière sécurisée auprès de votre API et de MadJoh.
- postFileAuth() envoie, comme postAuth, une request post authentifiée mais permet d'ajouter un fichier parmi vos arguments.

Pour pouvoir utiliser les fonctions de type postAuth et postFileAuth il faut donc que vos utilisateurs se soient connectés à leur compte.
Vous pouvez bien sûr construire vos propres modules pour communiquer avec l'API MadJoh. Vous pouvez inversement utiliser les modules MadJoh pour communiquer avec d'autres API que celle de MadJoh. Nous vous conseillons dans tous les cas de n'utiliser la méthode post que pour l'inscription et la connexion et de n'utiliser ensuite que les méthodes  postAuth et postFileAuth.

Sur ce tutoriel, nous vous montrerons comment construire pas a pas une application profitant au maximum des outils de MadJoh.
Dans ce premier chapitre nous verrons comment utiliser les modules MadJoh pour créer des comptes et s'y connecter facilement.